use std::process::exit;

mod builder;

fn main() {
	builder::profile::update();
	builder::analysis::check_state();
}
