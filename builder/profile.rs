
use std::env;
use std::fs::File;
use std::io::{Write, BufReader, BufRead};
use regex::Regex;
use std::fs::OpenOptions;

pub fn update() {
    let profile = String::from("[") + env::var("PROFILE").unwrap().as_str() + "]";
    let profile_re = String::from("\\[") + env::var("PROFILE").unwrap().as_str() + "\\]";
    let file = File::open("./bot_config/rez_onyx.cfg").unwrap();
    let reader = BufReader::new(file);
    let mut buffer = String::new();

    let mut build_found = false;
    let mut case_found = false;
    let mut replacement = String::new();
    let re_other: Regex = Regex::new(r"# .*$").unwrap();
    let re_case: Regex = Regex::new((String::from(r"# .*")+profile_re.as_str()+r" (?P<d>.*)$").as_str()).unwrap();
    for line in reader.lines() {
        let line = line.unwrap();
        if case_found {
            if re_other.is_match(line.as_str()) {
                buffer += line.as_str();
                buffer += "\n";
            }
            else {
                buffer += replacement.as_str();
                buffer += "\n";
                case_found = false;
                build_found = false;
            }
            continue;
        }
        else if line.contains("[build]") {
            build_found = true;
        }
        else if build_found && line.contains(profile.as_str()) {
            case_found = true;
            replacement = re_case.replace(line.as_str(), "$d").to_string();
        }
        buffer += line.as_str();
        buffer += "\n";
    }
    let mut file = OpenOptions::new()
        .write(true)
        .open("./bot_config/rez_onyx.cfg").unwrap();
    write!(file, "{}", buffer).unwrap();

}