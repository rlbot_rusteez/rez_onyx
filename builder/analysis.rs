
use std::env;
use std::fs::File;
use std::io::{self, Write, BufReader, BufRead};
use regex::Regex;
use std::fs::OpenOptions;
use chrono::{Utc, Datelike, Timelike};

use std::error::Error;
use std::io::prelude::*;
use std::path::PathBuf;
use glob::glob;
use std::process::exit;

fn grep<R>(path_string: &str, targets: &Vec<(&str, &str)>, reader: R) -> io::Result<String>
    where R: BufRead
{
    let mut file: Vec<(usize, String)> = vec![(0, "".to_string())];
    let mut file2 = reader.lines().enumerate().map(|line| {(line.0, line.1.unwrap())}).collect();
    file.append(&mut file2);
    file.push((0, "".to_string()));
    let mut buffer = String::new();
    for lines in file.as_slice().windows(3) {
        for target in targets {
            if lines[1].1.contains(target.0) {
                buffer += format!("\\x1b[1m\\x1b[38;2;253;188;75mwarning\\x1b[0m\\x1b[1m: {}\n  \\x1b[1m\\x1b[38;2;61;174;233m-->\\x1b[0m {}:{}:\n   \\x1b[1m\\x1b[38;2;61;174;233m|\\x1b[0m{}\n\\x1b[1m\\x1b[38;2;61;174;233m{:<3}|\\x1b[0m\\x1b[1m{}\\x1b[0m\n   \\x1b[1m\\x1b[38;2;61;174;233m|\\x1b[0m{}\n\n", target.1, path_string, lines[1].0+1, lines[0].1, lines[1].0+1, lines[1].1, lines[2].1).as_str();
            }
        }
    }
    Ok(buffer)
}

pub fn check_state() {
    let file = File::open("./src/the_state_of_it.rs").unwrap();
    let mut buffer = String::from("// auto-generated content. See rez_onyx/builder/analysis.rs .
// last update: ");
    let now = Utc::now();
    buffer += format!("{}", now.to_rfc3339()).as_str();
    buffer += "

pub fn issues() {
    println!(\"\n\n\\x1b[38;2;253;188;75m      • —————  Codebase status check  ————— •      \\x1b[0m\n\n";


    let mut lines_buffer = String::new();
    let targets = vec![("!!!", "Potentially problematic code"), (".unwrap()", "Wild unwrapping")];
    for file in glob("src/**/*.rs").expect("Failed to read glob pattern") {
        let file = file.unwrap();
        let path_string = file.to_str().unwrap();
        if path_string.contains("the_state_of_it.rs") {continue}
        let f = File::open(file.clone()).expect("Couldn't open file");
        let buf = grep(path_string, &targets, BufReader::new(f)).unwrap();

        lines_buffer += buf.as_str();
    }

    if lines_buffer.len() == 0 {
       buffer += "\\x1b[38;2;78;220;81m⇒ The code is fine!\\x1b[0m";
    }
    else {
        lines_buffer = str::replace(lines_buffer.clone().as_str(), "{", "{{");
        lines_buffer = str::replace(lines_buffer.clone().as_str(), "}", "}}");
        lines_buffer = str::replace(lines_buffer.clone().as_str(), "\"", "\\\"");
        buffer += &lines_buffer;
        buffer += "\\x1b[38;2;61;174;233m    ⇒ There are some issues to address.\\x1b[0m\n";
    }
    buffer += "\\x1b[38;2;253;188;75m      • ————————————————————————————— •      \\x1b[0m\n\");
}";
    let mut file = OpenOptions::new()
        .write(true)
        .truncate(true)
        .open("./src/the_state_of_it.rs").unwrap();
    write!(file, "{}", buffer).unwrap();

}

