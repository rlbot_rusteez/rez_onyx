// auto-generated content. See rez_onyx/builder/analysis.rs .
// last update: 2020-09-28T22:20:21.640425494+00:00

pub fn issues() {
    println!("

\x1b[38;2;253;188;75m      • —————  Codebase status check  ————— •      \x1b[0m

\x1b[1m\x1b[38;2;253;188;75mwarning\x1b[0m\x1b[1m: Wild unwrapping
  \x1b[1m\x1b[38;2;61;174;233m-->\x1b[0m src/cockpit/copilot/mod.rs:57:
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m                    RacerState::Play(context) => {{
\x1b[1m\x1b[38;2;61;174;233m57 |\x1b[0m\x1b[1m                        let pos = &context.ball.as_ref().unwrap().physics.location;\x1b[0m
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m                        for pilot in &self.pilots {{

\x1b[1m\x1b[38;2;253;188;75mwarning\x1b[0m\x1b[1m: Wild unwrapping
  \x1b[1m\x1b[38;2;61;174;233m-->\x1b[0m src/cockpit/copilot/mod.rs:63:
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m                    RacerState::Ponder(context) => {{
\x1b[1m\x1b[38;2;61;174;233m63 |\x1b[0m\x1b[1m                        let pos = &context.ball.as_ref().unwrap().physics.location;\x1b[0m
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m                        for pilot in &self.pilots {{

\x1b[1m\x1b[38;2;253;188;75mwarning\x1b[0m\x1b[1m: Wild unwrapping
  \x1b[1m\x1b[38;2;61;174;233m-->\x1b[0m src/cockpit/pilot/aim.rs:100:
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m
\x1b[1m\x1b[38;2;61;174;233m100|\x1b[0m\x1b[1m        group.render().unwrap();*/\x1b[0m
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m

\x1b[1m\x1b[38;2;253;188;75mwarning\x1b[0m\x1b[1m: Wild unwrapping
  \x1b[1m\x1b[38;2;61;174;233m-->\x1b[0m src/cockpit/pilot/mod.rs:67:
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m            // Sort of a \"take the last message\" system
\x1b[1m\x1b[38;2;61;174;233m67 |\x1b[0m\x1b[1m            let mut tmp_msg= self.source.recv().unwrap();\x1b[0m
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m            let mut count = 0;

\x1b[1m\x1b[38;2;253;188;75mwarning\x1b[0m\x1b[1m: Wild unwrapping
  \x1b[1m\x1b[38;2;61;174;233m-->\x1b[0m src/cockpit/pilot/mod.rs:134:
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m
\x1b[1m\x1b[38;2;61;174;233m134|\x1b[0m\x1b[1m        let (controller, lines) = aim::pteam_saving(packet, &target, index).unwrap().unwrap();\x1b[0m
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m        //say!(\"location1: {{}} -> {{}}\", target.location.position[1], (target.location.position[1] / 4096.) as f32);

\x1b[1m\x1b[38;2;253;188;75mwarning\x1b[0m\x1b[1m: Potentially problematic code
  \x1b[1m\x1b[38;2;61;174;233m-->\x1b[0m src/main.rs:66:
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m        let bot = bot_builder
\x1b[1m\x1b[38;2;61;174;233m66 |\x1b[0m\x1b[1m            .name(\"Onyx\" /* !!! possible issue? !!! */)\x1b[0m
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m            .index(index)

\x1b[1m\x1b[38;2;253;188;75mwarning\x1b[0m\x1b[1m: Wild unwrapping
  \x1b[1m\x1b[38;2;61;174;233m-->\x1b[0m src/main.rs:74:
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m            collin.start();
\x1b[1m\x1b[38;2;61;174;233m74 |\x1b[0m\x1b[1m        }}).unwrap();\x1b[0m
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m        let petra_thread = thread::Builder::new().name(petra.name() + \" \" + index.to_string().as_str()).spawn(move|| {{

\x1b[1m\x1b[38;2;253;188;75mwarning\x1b[0m\x1b[1m: Wild unwrapping
  \x1b[1m\x1b[38;2;61;174;233m-->\x1b[0m src/main.rs:77:
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m            petra.start();
\x1b[1m\x1b[38;2;61;174;233m77 |\x1b[0m\x1b[1m        }}).unwrap();\x1b[0m
   \x1b[1m\x1b[38;2;61;174;233m|\x1b[0m

\x1b[38;2;61;174;233m    ⇒ There are some issues to address.\x1b[0m
\x1b[38;2;253;188;75m      • ————————————————————————————— •      \x1b[0m
");
}