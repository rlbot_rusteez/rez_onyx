use rez_onyx_lib as onyx_lib;
use rusteez::{self, cockpit::{Mate, Racer}, context::{self, Context, Contexts}};
use std::env::args;
use log::{info, error, warn, LevelFilter};
use std::thread;
use std::borrow::Borrow;
use std::hint::unreachable_unchecked;
use rlbot::{Bot, MatchSettings, PlayerConfiguration, PlayerClass};

mod the_state_of_it;

fn main() {


    // Setup the control tools (logging, console, …)
    #[cfg(feature = "mission-control")]
    rusteez::mission_control::setup(None, vec![("rusteez", LevelFilter::Debug)]);/*Some(LevelFilter::Info));*/
    #[cfg(feature = "mission-control")]
    the_state_of_it::issues();

    info!(" ——— [REZ] Onyx ——— ");
    info!("Starting up the context");

    // Is the bot run by a framework?
    let (context, mut args) = context::rlbot::Play::try_new_from_args(args()).unwrap_or_else(|err| {panic!("Couldn't build the detected context: {}", err)});

    // if not, start a game
    let (mut context, indexes) = if context.is_err() {
        let (mut ctx, new_args) = context::rlbot::Play::try_new(args).expect("Couldn't create the context");
        args = new_args;

        let mut settings = MatchSettings::rlbot_vs_rlbot("Onyx", "Onyx");
        settings.player_configurations = vec![
            PlayerConfiguration::new(PlayerClass::RLBotPlayer, "Onyx", 0),
            PlayerConfiguration::new(PlayerClass::RLBotPlayer, "Onyx", 0),
            PlayerConfiguration::new(PlayerClass::RLBotPlayer, "Onyx", 0),

            PlayerConfiguration::new(PlayerClass::RLBotPlayer, "Onyx", 1),
            PlayerConfiguration::new(PlayerClass::RLBotPlayer, "Onyx", 1),
            PlayerConfiguration::new(PlayerClass::RLBotPlayer, "Onyx", 1),
        ];

        ctx.start_match(&settings);
        (Contexts::RLBotPlay(ctx), vec![0, 1, 2, 3, 4, 5])
    }
    else {context.unwrap(/*checked in the condition*/)};

    if indexes.len() > 1 {info!("Starting up the bots")}
        else {info!("Starting up the bot")};

    let mut bots = Vec::new();
    let mut handles = Vec::new();

    for index in indexes {
        let mut bot_builder = rusteez::BotBuilder::new();

        // Create the racers
        let mut collin = onyx_lib::copilot::Collin::new(&mut bot_builder);
        let mut petra = onyx_lib::pilot::Petra::new(&mut bot_builder);

        // Link the Pilot to the Copilot
        collin.add_pilot_mate(&mut petra);

        // Parse arguments and set up context
        let bot = bot_builder
            .name("Onyx" /* !!! possible issue? !!! */)
            .index(index)
            .build();

        info!("{} here! \x1b[38;5;214m⌟\x1b[38;5;202m⎛•‿•⎞\x1b[38;5;214m⌞\x1b[0m  I'm ready!", bot.id());

        let collin_thread = thread::Builder::new().name(collin.name() + " " + index.to_string().as_str()).spawn(move|| {
            collin.start();
        }).unwrap();
        let petra_thread = thread::Builder::new().name(petra.name() + " " + index.to_string().as_str()).spawn(move|| {
            petra.start();
        }).unwrap();

        bots.push(bot);
        handles.push((collin_thread, petra_thread));
    }

    // Run the context
    context.run(bots);


    info!("Waiting for the racers to finish");
    for (copilot_thread, pilot_thread) in handles {
        let copilot_state = copilot_thread.join();
        let pilot_state = pilot_thread.join();
        info!("Termination handles: Copilot: {:?}, Pilot: {:?}", copilot_state, pilot_state);
    }
}
