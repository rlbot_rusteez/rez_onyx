use rlbot::{ControllerState, GameTickPacket, RenderGroup};
use na::{Vector2, Vector3};
use std::error::Error;
use rusteez::world::physics::Position;
use rusteez::say;
use rusteez::context::RacerShow;
use log::{warn, debug};
use std::cmp::Ordering;

pub fn pteam_saving(packet: &GameTickPacket, position: &Position, pcar_idx: usize/*, group: RenderGroup*/) -> Option<Option<(rlbot::ControllerState,Vec<RacerShow>)>> /*todo: invert sign of the printed data when on orange team*/ {
    let ball = packet.ball.as_ref();
    let player_car = packet.players.get(pcar_idx);

    if let (Some(ball), Some(player_car)) = (ball, player_car) {

        let (ball_l2d, player_car_l2d,
             ball_v2d, player_car_v2d,
             ball_r1d, player_car_r1d,
             backboard_y) = match player_car.team {
            // Blue team
            0 => {
                (Vector2::new(ball.physics.location.x, ball.physics.location.y), Vector2::new(player_car.physics.location.x, player_car.physics.location.y),
                 Vector2::new(ball.physics.velocity.x, ball.physics.velocity.y), Vector2::new(player_car.physics.velocity.x, player_car.physics.velocity.y),
                 ball.physics.rotation.yaw, player_car.physics.rotation.yaw,
                 -5120f32)
            },
            // Orange team
            1 => {
                (Vector2::new(-ball.physics.location.x, -ball.physics.location.y), Vector2::new(-player_car.physics.location.x, -player_car.physics.location.y),
                 Vector2::new(-ball.physics.velocity.x, -ball.physics.velocity.y), Vector2::new(-player_car.physics.velocity.x, -player_car.physics.velocity.y),
                 -ball.physics.rotation.yaw, -player_car.physics.rotation.yaw,
                 5120f32)
            },
            _ => unreachable!("team value not understood: {}
It's supposed to be either 0 or 1.", player_car.team)
        };
        /*
        // Locations
        let ball_loc_2d = Vector2::new(ball.physics.location.x, ball.physics.location.y);
        let player_car_loc_2d = Vector2::new(player_car.physics.location.x, player_car.physics.location.y);

        // Velocities
        let ball_vel_2d = Vector2::new(ball.physics.velocity.x, ball.physics.velocity.y);
        let player_car_vel_2d = Vector2::new(player_car.physics.velocity.x, player_car.physics.velocity.y);

        let backboard_y = if ball_vel_2d.y < 0. { -5120f32 } else { 5120f32 };
        /*match pcar.team {
        // Blue team
        0 => -5120f32,
        // Orange team
        1 => 5120f32,
        a => unreachable!("team value not understood: {}", a)
    };*/
        */
        let dist_ball_backboard_y = backboard_y - ball_l2d.y;
        /*let ratio_x_y = dist_ball_backboard/ball_vel_2d.y;
    let intersection_offset_x = ball_vec_2d.x*ratio_x_y;
    let intersection_backboard_ball_x = ball_loc_2d.x + intersection_offset_x;*/
        let intersection_backboard_ball_x = (dist_ball_backboard_y * ball_v2d.x / ball_v2d.y) + ball_l2d.x;

        // Calculate the position of the intersection between the goal-line and the ball's trajectory
        //
        /*let ball_vel_2d_y_unit = ball_vel_2d.y / (ball_vel_2d.x.powi(2) + ball_vel_2d.y.powi(2)).sqrt();
    let ball_impact_dist = (5120f32 - ball_loc_2d.y) / ball_vel_2d_y_unit;
    let goal_line_ball_traj_intersection = ball_loc_2d + ball_vel_2d * ball_impact_dist;
    if (-893f32..893f32).contains(&(goal_line_ball_traj_intersection.x)) {*/


        // shot on goal?
        /*if (-893f32..893f32).contains(&(intersection_backboard_ball_x)) {
            // own goal?
            if (player_car.team == 0 && ball_v2d.y < 0.) || (player_car.team == 1 && ball_v2d.y > 0.) {
                //say!("Uh oh! :/ {:?}", player_car.team);
                //return Some(None);
                if is_home(player_car_l2d) {
                    return Some(Some(towards_ball(player_car_l2d, player_car_v2d, ball_l2d)))
                }
                return Some(Some({let mut input = back_home(player_car_l2d, player_car_v2d); input.boost = true; input}))
            }
        }*/


        /*if ball_l2d.y < player_car_l2d.y {
            debug!("BH");
            Some(Some(back_home(player_car_l2d, player_car_r1d)))
        }
        else {
            debug!("TB");*/
            Some(Some((towards_ball(player_car_l2d, player_car_r1d, ball_l2d))))
        //}
        /*let mut group = group;
        let green = group.color_rgb(0, 255, 0);
        let red = group.color_rgb(255, 0, 0);

        //group.draw_string_2d((10.0, 10.0), (2, 2), "I am text!", green);
        group.draw_line_3d((ball.physics.location.x, ball.physics.location.y, 50.), (intersection_backboard_ball_x, backboard_y, 50.), green);
        group.draw_line_3d((-893f32, -5120f32, 50.), (-893f32, 5120f32, 50.), red);
        group.draw_line_3d((893f32, -5120f32, 50.), (893f32, 5120f32, 50.), red);

        group.render().unwrap();*/

        //println!("{:?} ({:?})", ball_impact_g2d, pcar.team);
        //Some(None)
    }
    else {
        warn!("Help! I'm lost!    ——— No state information. Sending default throttle input.");
        Some(Some((rlbot::ControllerState {
            throttle: 1.0,
            steer: 0.0,
            pitch: 0.0,
            yaw: 0.0,
            roll: 0.0,
            boost: false,
            ..Default::default()
        }, vec![])))
    }
}


fn towards_ball(car_l2d: Vector2<f32>, car_r1d: f32, ball_l2d: Vector2<f32>) -> (rlbot::ControllerState, Vec<RacerShow>) {
    debug!("INPUT: Towards ball {}", car_r1d);
    let dir: Vector2<f32> = car_l2d - ball_l2d;

    let car_dir = dir.normalize().dot(&Vector2::new(car_r1d.cos(), car_r1d.sin()));

    // Rotate towards ball

    let turn_dir =
    if true/**/ { // Left turn smaller
        (-car_dir + 1.) / 2.
    }
    else { // Right …
        -(-car_dir + 1.) / 2.
    };

    let dir_norm = dir.normalize();
    let ball_dir_r1d = (dir_norm.y/dir_norm.x).atan();

    let turn_direction = car_r1d - ball_dir_r1d;


    (rlbot::ControllerState {
            throttle: 0.1,//float_norm_clamp(float_max((turn_direction.abs()), 0.1)),
            steer: float_norm_clamp(-(turn_direction)),
            pitch: 0.0,
            yaw: 0.0,
            roll: 0.0,
            boost: false,
            handbrake: true,//car_dir.abs() < 0.1,
            ..Default::default()
        }, vec![RacerShow::Render3DLineBegin,
                RacerShow::Render3DLine(Vector3::new(0., 0., 50.), Vector3::new(ball_l2d.x, ball_l2d.y, 50.), Vector3::new(255,255,0)),
                RacerShow::Render3DLineEnd])
}

fn back_home(car_l2d: Vector2<f32>, car_r1d: f32) -> rlbot::ControllerState {
    debug!("INPUT: Back home");

    //[behaviour] will send it flying most probably
    let x = if car_l2d.x < -893.0 {-893.0} else if car_l2d.x > 893.0 {893.0} else {car_l2d.x};
    let closest_goal_point = Vector2::new(x, -5120f32);

    // Rotate outwards

    let dir =
    if car_l2d.x.is_sign_negative() { // Car's on the right half of the field
        (car_r1d.sin() + 1.) / 2.
    }
    else { // … left
        (car_r1d.sin() + 1.) / 2.
    };


    rlbot::ControllerState {
        throttle: 1.0,
        steer: float_norm_clamp(dir),
        pitch: 0.0,
        yaw: float_norm_clamp(dir),
        roll: 0.0,
        boost: true,
        handbrake: car_r1d.sin() < 0.1,
        ..Default::default()
    }
}

fn is_home(car_l2d: Vector2<f32>) -> bool {
    car_l2d.x < 893.0 && car_l2d.y < 50.0
}


fn float_max(a: f32, b: f32) -> f32 {
    if a.partial_cmp(&b).unwrap_or(Ordering::Equal) == Ordering::Greater {a} else {b}
}

fn float_min(a: f32, b: f32) -> f32 {
    if a.partial_cmp(&b).unwrap_or(Ordering::Equal) == Ordering::Less {a} else {b}
}

fn float_clamp(v: f32, a: f32, b: f32) -> f32 {
    if v.partial_cmp(&b).unwrap_or(Ordering::Equal) == Ordering::Greater {return b};
    if v.partial_cmp(&a).unwrap_or(Ordering::Equal) == Ordering::Less {return a};
    v
}

fn float_norm_clamp(v: f32) -> f32 {
    float_clamp(v, -1.0, 1.0)
}