mod aim;

use std::sync::mpsc::{self, Sender, Receiver};
use rusteez::{self, context::{RacerState, RacerShout, Radio, RacerShow}, BotBuilder};
use rusteez::cockpit::{Racer, Mate};
use crate::cockpit::{Message, Msg};
use rusteez::cockpit::pilot::Pilot;
use rusteez::cockpit::copilot::Copilot;
use log::{trace, info, debug};
use rusteez::say;
use rusteez::context::Input;
use rlbot::{GameTickPacket, ControllerState};
use rusteez::world::physics::{Position};
use std::thread;
use std::time::Duration;
use rusteez::mission_control::logging::scale_bad;
use rand::{thread_rng, Rng};


pub struct Petra {
    source: Receiver<Radio<Message, ()>>,
    //[feat] context: Sender<RacerShout>,
    source_copilots: Vec<Receiver<Message>>,
    //[feat] pilots: Vec<Sender<Radio>>,
    //[feat] copilots: Vec<Senders<Radio>>
    destination: Sender<Radio<Message, ()>>
}

impl Petra {
    pub fn new(bot_builder: &mut BotBuilder<Message, ()>) -> Petra {

        Petra {
            source: bot_builder.new_racer_channel(),
            destination: bot_builder.new_decider_channel(),
            source_copilots: vec![],
        }
    }
}

impl Mate<Message> for Petra {
    fn add_pilot_mate(&mut self, pilot: &mut impl Mate<Message>) {
        unimplemented!()
    }

    fn add_copilot_mate(&mut self, copilot: &mut impl Mate<Message>) {
        unimplemented!()
    }

    fn add_pilot_source(&mut self, pilot: Receiver<Message>) {
        unimplemented!()
    }

    fn add_copilot_source(&mut self, copilot: Receiver<Message>) {
        self.source_copilots.push(copilot);
    }
}

impl Racer<Message, ()> for Petra {
    fn start(&mut self) {
        trace!("Petra here!");
        let mut last_target = Position::default();
        loop {
            //trace!("get msg");
            let msg = self.source_copilots[0].try_recv();
            //trace!("get state");
            // Sort of a "take the last message" system
            let mut tmp_msg= self.source.recv().unwrap();
            let mut count = 0;
            let state = loop {
                if let Ok(message) = self.source.try_recv() {tmp_msg = message} //[caution] If this is slower than self.source sending the packets (which it won't be), packets will be dropped continuously
                else {break tmp_msg;}
                count += 1;
            };
            if count > 0 {
                info! ("{} jumped {} messages", scale_bad("[lag]", 0., 10., count as f64, /*((count.min(10) as f64)/20.)+0.5*/), count);
            }

            if let Radio::Context(message, index) = state {
                match &*message {
                    RacerState::Play(state) => {
                        //say!("time remaining {}", state.game_info.game_time_remaining);
                        //say!("playing");
                        if let Ok(msg) = msg {
                            if let Msg::Target(message) = msg.msg {
                                let (input, lines) = self.tick(&*state, &message, index);
                                //say!("sending");
                                for line in lines {
                                    debug!("{:?}", line);
                                    self.destination.send(Radio::Show(line, index));
                                }
                                self.destination.send(Radio::Decision(input, index));
                                last_target = message;
                            }
                        }
                        else {
                            let (input, lines) = self.tick(&*state, &last_target, index);
                            //say!("sending");
                            for line in lines {
                                self.destination.send(Radio::Show(line, index));
                            }
                            self.destination.send(Radio::Decision(input, index));

                        }
                    },
                    RacerState::Ponder(state) => {
                    },
                    RacerState::Pause => {
                    },
                    _ => todo!("manage other states")
                };
            }



        }

    }

    fn name(&self) -> String {
        String::from("Petra")
    }

    fn world_source(&mut self, source: Receiver<Radio<Message, ()>>) {
        self.source = source;
    }
}


impl Petra {
    //fn tick(&mut self, packet: &GameTickPacket) -> Input {
    fn tick(&mut self, packet: &GameTickPacket, target: &Position, index: usize) -> (Input, Vec<RacerShow>) {
        let mut steer= 0.;

        let (controller, lines) = aim::pteam_saving(packet, &target, index).unwrap().unwrap();
        //say!("location1: {} -> {}", target.location.position[1], (target.location.position[1] / 4096.) as f32);

        //thread::sleep(Duration::from_millis(thread_rng().gen_range::<u64, _, _>(0, 20).pow(2)));
        (Input::new(controller), lines)

        /*ControllerState {
            throttle: 1.0,
            steer: (target.location.position[1] / 5120.) as f32,
            ..Default::default()
        })*/
    }
}

impl Pilot for Petra {

}


impl Petra {
    fn run() -> Result<(), ()> {
        Ok(())
    }
}
