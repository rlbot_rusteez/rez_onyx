use std::sync::mpsc::{self, Sender, Receiver};
use rusteez::{self, context::{RacerState, RacerShout, Radio}, BotBuilder};
use rusteez::cockpit::{Racer, Mate};
use crate::cockpit::{Message, Msg};
use rusteez::cockpit::pilot::Pilot;
use rusteez::cockpit::copilot::Copilot;
use log::{trace, info};
use std::sync::Arc;

pub struct Collin {
    source: Receiver<Radio<Message, ()>>,
    //[feat] context: Sender<RacerShout>,
    pilots: Vec<Sender<Message>>,
    //[feat] copilots: Vec<Senders<Radio>>
}

impl Collin {
    pub fn new(bot_builder: &mut BotBuilder<Message, ()>) -> Collin {

        Collin {
            source: bot_builder.new_racer_channel(),
            pilots: vec![]
        }
    }
}

impl Mate<Message> for Collin {
    fn add_pilot_mate(&mut self, pilot: &mut impl Mate<Message>) {
        let (sender, receiver) = mpsc::channel();
        self.pilots.push(sender);
        pilot.add_copilot_source(receiver);

    }

    fn add_copilot_mate(&mut self, copilot: &mut impl Mate<Message>) {
        unimplemented!()
    }

    fn add_pilot_source(&mut self, pilot: Receiver<Message>) {
        unimplemented!()
    }

    fn add_copilot_source(&mut self, copilot: Receiver<Message>) {
        unimplemented!()
    }
}

impl Racer<Message, ()> for Collin {
    fn start(&mut self) {
        trace!("Collin here!");

        loop {
            let message = self.source.recv().expect("Couldn't get source message");
            if let Radio::Context(state, index) = message {
                match &*state {
                    RacerState::Play(context) => {
                        let pos = &context.ball.as_ref().unwrap().physics.location;
                        for pilot in &self.pilots {
                            pilot.send(Message { id: 0, msg: Msg::Target(pos.into()) });
                        }
                    },
                    RacerState::Ponder(context) => {
                        let pos = &context.ball.as_ref().unwrap().physics.location;
                        for pilot in &self.pilots {
                            pilot.send(Message {id: 0, msg: Msg::Target(pos.into())});
                        }
                    },
                    RacerState::Pause => {},
                    RacerState::Stop => {},
                    _ => todo!("implement other states")
                }
            }


                                /*
            if let Radio::Context(message) = message {
                match message {
                    RacerState::Play(state) | RacerState::Ponder(state) => {
                        say!("time remaining {}", state.game_info.game_time_remaining);
                        let input = self.tick(&*state);
                        self.destination.send(Radio::Decision(input));
                    },
                    _ => todo!("manage other states")
                };
            }*/

        }
    }

    fn name(&self) -> String {
        String::from("Collin")
    }

    fn world_source(&mut self, source: Receiver<Radio<Message, ()>>) {
        self.source = source;
    }
}

impl Copilot for Collin {

}


impl Collin {
    fn run() -> Result<(), ()> {
        Ok(())
    }
}