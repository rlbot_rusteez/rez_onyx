pub mod copilot;
pub mod pilot;
pub mod mission_control;

use rusteez::world::physics::Position;
use rusteez::context::Radio;

#[derive(Clone, PartialEq, Default, Debug)]
pub struct Message {
    pub id: usize,
    pub msg: Msg
}

#[derive(Clone, PartialEq, Debug)]
pub enum Msg {
    Target(/*target_position:*/ Position),
    Needs(/*time_in_secs:*/ usize),
    Ack
}

//[tmp] default should not exist
impl Default for Msg {
    fn default() -> Self {
        Msg::Ack
    }
}