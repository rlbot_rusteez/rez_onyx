mod cockpit;
pub use cockpit::copilot;
pub use cockpit::pilot;
pub use cockpit::mission_control;